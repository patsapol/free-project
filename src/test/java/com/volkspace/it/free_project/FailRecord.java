package com.volkspace.it.free_project;

import org.json.simple.JSONObject;

public class FailRecord {
	
	private JSONObject data;
	
	private String row_number;
	
	private String reason;

	public FailRecord() {
	}

	public JSONObject getData() {
		return data;
	}

	public void setData(JSONObject data) {
		this.data = data;
	}

	public String getRow_number() {
		return row_number;
	}

	public void setRow_number(String row_number) {
		this.row_number = row_number;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Override
	public String toString() {
		return "FailRecord [data=" + data + ", row_number=" + row_number + ", reason=" + reason + "]";
	}
	
}
