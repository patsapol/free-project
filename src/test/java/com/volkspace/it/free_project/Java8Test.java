package com.volkspace.it.free_project;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;

public class Java8Test {
	List<User> users = Arrays.asList(
		    new User(1, "Test 01", 12, "male"),
		    new User(2, "Test 02", 20, "famale"),
		    new User(3, "Test 03", 30, "male"),
		    new User(4, "Test 04", 30, "famale"),
		    new User(5, "Test 05", 20, "famale")
		);
	
	//@Test
	public void ageAvg() {
		List<String> list = Arrays.asList("Anakin", "Luke", "Darth Vader", "Han Solo", "Stormtrooper", "Cherprang", null);
		
		Optional<List<String>> listOptional = createOptional(null);
		System.out.println(listOptional.toString());
	}

	private static <T> Optional<T> createOptional(T value) {
        return value == null ? Optional.empty() : Optional.of(value);
    }	
	
	@Test
	public void testString() {
		String duke = new String("duke");
		String duke2 = new String("duke");

		System.out.println(duke == duke2);
		System.out.println(duke.intern() == duke2.intern());
		System.out.println(new String("duke").trim() == new String("duke").trim());
	}
}
