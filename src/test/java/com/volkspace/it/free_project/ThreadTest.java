package com.volkspace.it.free_project;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class MyThread implements Runnable {

	public int threadNum;
	
	public static int number = 1;
	public static int MAX_NUMBER = 100;
	
	public MyThread(int threadNum) {
        this.threadNum = threadNum;
    }

	@Override
	public void run() {
		System.out.println("MyThread running... : "+ threadNum);
		while(true) {
			int n = ++number;
			if(n <= MAX_NUMBER) {
				if(IsPrime(n)) {
					System.out.println("Thread " + threadNum + ": " + n + " is prime number");
				}
			} else {
				break;
			}
		}
	}
	
	public boolean IsPrime(int number) {
	    for (int i = 2; i < number; i++) {
	        if (number % i == 0 && i != number) return false;
	    }
	    return true;
	}
}

public class ThreadTest {

	public static void main(String[] args) {
		for(int index=0;index<5;index++) {
			Thread t1 = new Thread(new MyThread(index+1));  
	        t1.start();
        }
        
        Runnable task = () -> System.out.println("Task e1 running... : "+ Thread.currentThread().getName());
        new Thread(task).start();
        
        Executor e = Executors.newSingleThreadExecutor();
        e.execute(task);
        
        Runnable task2 = () -> {
        		System.out.println("Task e2 running... : "+ Thread.currentThread().getName());
        };
        	
        Executor e2 = Executors.newCachedThreadPool();
        e2.execute(task2);
        
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            String threadName = Thread.currentThread().getName();
            System.out.println("Hello " + threadName);
        });
        executor.shutdownNow();
        
        ExecutorService es = Executors.newWorkStealingPool();

        List<Callable<String>> callables = Arrays.asList(
                () -> "task1",
                () -> "task2",
                () -> "task3");

        try {
        		es.invokeAll(callables)
			    .stream()
			    .map(future -> {
			        try {
			            return future.get();
			        }
			        catch (Exception e1) {
			            throw new IllegalStateException(e1);
			        }
			    })
			    .forEach(System.out::println);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
}