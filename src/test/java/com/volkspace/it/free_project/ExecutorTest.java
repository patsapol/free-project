package com.volkspace.it.free_project;

import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.StampedLock;
import java.util.stream.IntStream;

import org.junit.Test;

public class ExecutorTest {

	//@Test
	public void runExecutor() throws InterruptedException {
		ScheduledExecutorService ex = Executors.newScheduledThreadPool(1);
		
		Runnable task = () -> System.out.println("Scheduled nano : "+ System.nanoTime());
			
		ScheduledFuture<?> future = ex.schedule(task, 3, TimeUnit.SECONDS);
			
		TimeUnit.MILLISECONDS.sleep(1337);

		long remainingDelay = future.getDelay(TimeUnit.MILLISECONDS);
		System.out.printf("Remaining Delay: %sms", remainingDelay);
		
		TimeUnit.MILLISECONDS.sleep(1661);
	}
	
	//@Test
	public void runExecutor2() throws InterruptedException {
		ScheduledExecutorService ex = Executors.newScheduledThreadPool(1);
		
		Runnable task = () -> {
			System.out.println(Thread.currentThread().getName()+" Start. Time = "+ new Date());
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        System.out.println(Thread.currentThread().getName()+" End. Time = "+ new Date());
		};
		
		ex.scheduleAtFixedRate(task, 1, 1, TimeUnit.SECONDS);
		Thread.sleep(10000);
	}
	
	//@Test
	public void runExecutor3() throws InterruptedException {
		ScheduledExecutorService ex = Executors.newScheduledThreadPool(1);
		
		Runnable task = () -> {
			System.out.println(Thread.currentThread().getName()+" Start. Time = "+ new Date());
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        System.out.println(Thread.currentThread().getName()+" End. Time = "+ new Date());
		};
		
		ex.scheduleWithFixedDelay(task, 1, 3L, TimeUnit.SECONDS);
		Thread.sleep(10000);
	}
	
	ReentrantLock lock = new ReentrantLock();
	int count = 0;

	void increment() {
	    lock.lock();
	    try {
	        count++;
	    } finally {
	        lock.unlock();
	    }
	}
	
	//@Test
	public void runSync() {
		ExecutorService executor = Executors.newFixedThreadPool(2);
		IntStream.range(0, 10000).forEach(i -> executor.submit(this::increment));
		stop(executor);
		System.out.println(count);  
	}
	
	//@Test
	public void lockTest() {
		ExecutorService executor = Executors.newFixedThreadPool(2);
		ReentrantLock lock = new ReentrantLock();

		executor.submit(() -> {
			System.out.println("date : "+ new Date());
		    lock.lock();
		    System.out.println("Held by me1: " + lock.isHeldByCurrentThread());
		    try {
		        sleep(1);
		    } finally {
		        lock.unlock();
		    }
		    System.out.println("date1 : "+ new Date());
		});

		executor.submit(() -> {
			System.out.println("date2 : "+ new Date());
		    System.out.println("Locked: " + lock.isLocked());
		    System.out.println("Held by me: " + lock.isHeldByCurrentThread());
		    boolean locked = lock.tryLock();
		    System.out.println("Lock acquired: " + locked);
		    System.out.println("date22 : "+ new Date());
		});

		stop(executor);
	}
	
	//@Test
	public void stampedLock() {
		ExecutorService executor = Executors.newFixedThreadPool(2);
		StampedLock lock = new StampedLock();

		executor.submit(() -> {
		    long stamp = lock.readLock();
		    try {
		        if (count == 0) {	
		            stamp = lock.tryConvertToWriteLock(stamp);
		            System.out.println("Stamp : "+ stamp);
		            if (stamp == 0L) {
		                System.out.println("Could not convert to write lock");
		                stamp = lock.writeLock();
		            }
		            count = 23;
		        }
		        System.out.println(count);
		    } finally {
		        lock.unlock(stamp);
		    }
		});

		stop(executor);
	}
	
	//@Test
	public void semephore() {
		ExecutorService executor = Executors.newFixedThreadPool(10);

		Semaphore semaphore = new Semaphore(7);

		Runnable longRunningTask = () -> {
		    boolean permit = false;
		    try {
		        permit = semaphore.tryAcquire(1, TimeUnit.SECONDS);
		        System.out.println("Permit : "+ permit);
		        if (permit) {	
		            System.out.println("Semaphore acquired");
		            sleep(5);
		        } else {
		            System.out.println("Could not acquire semaphore");
		        }
		    } catch (InterruptedException e) {
		        throw new IllegalStateException(e);
		    } finally {
		        if (permit) {
		            semaphore.release();
		        }
		    }
		};

		IntStream.range(0, 10)
		    .forEach(i -> executor.submit(longRunningTask));

		stop(executor);
	}
	
	@Test
	public void evenOrAdd() {
		int[] array = {2, 5, 34, 6, 0};
		long count = Arrays.stream(array).filter(s -> s != 0).count();
	    if(count == 0) System.out.println("[0]");;
	    if(count % 2 == 0) {
	    		System.out.println("odd");
	    } else {
	    		System.out.println("even");
	    }
	}
	
	public static void stop(ExecutorService executor) {
        try {
            executor.shutdown();
            executor.awaitTermination(60, TimeUnit.SECONDS);
        }
        catch (InterruptedException e) {
            System.err.println("termination interrupted");
        }
        finally {
            if (!executor.isTerminated()) {
                System.err.println("killing non-finished tasks");
            }
            executor.shutdownNow();
        }
    }

    public static void sleep(int seconds) {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }
	
}
