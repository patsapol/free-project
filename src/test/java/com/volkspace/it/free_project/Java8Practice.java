package com.volkspace.it.free_project;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;

public class Java8Practice {
	List<User> users = Arrays.asList(
			new User(1, "Folk1", 14, "male"),
			new User(2, "Folk2", 16, "female"),
			new User(3, "Folk3", 18, "male"),
			new User(4, "Folk4", 24, "female")
			);
	
	@Test
	public void allCase() {
		System.out.println("foreach age : ");
		users.stream().mapToInt(User::getAge).forEach(System.out::println);
		
		String firstChild = users.stream().findFirst().get().toString();
		System.out.println("firstChild : "+ firstChild);
		
		Predicate<User> user1 = u -> u.getAge() == 14;
		boolean checkall = users.stream().allMatch(user1);
		boolean checkany = users.stream().anyMatch(user1);
		boolean checknone = users.stream().noneMatch(user1);
		System.out.println("checkall : "+ checkall);
		System.out.println("checkany : "+ checkany);
		System.out.println("checknone : "+ checknone);
			
		double avg = users.stream().mapToInt(User::getAge).average().getAsDouble();
		System.out.println("avg : "+ avg);
		
		double sum = users.stream().mapToInt(User::getAge).sum();
		System.out.println("sum : "+ sum);
		
		//Stream.reduce() with Accumulator
		users.stream().mapToInt(User::getAge).reduce((x,y) -> x+y).ifPresent(s -> System.out.println("sumReduce - Accumulator : "+ s));
		users.stream().mapToInt(User::getAge).reduce(Integer::sum).ifPresent(s -> System.out.println("sumReduce - Accumulator : "+ s));
		
		//Stream.reduce() with Identity and Accumulator
		int sumreduce = users.stream().mapToInt(User::getAge).reduce(0, (x,y) -> x+y);
		int sumreduce2 = users.stream().mapToInt(User::getAge).reduce(0, Integer::sum);
		System.out.println("sumreduce : "+ sumreduce);
		System.out.println("sumreduce2 : "+ sumreduce2);
		
		//Stream.reduce() with Identity, Accumulator and Combiner
		List<Integer> list = Arrays.asList(2, 3, 4);
		int res = list.parallelStream().reduce(2, (s1,s2) -> s1*s2, (p,q) -> p+q);
		System.out.println("res : "+ res);
		
		System.out.println("foreach only female : ");
		users.stream().filter(p -> p.getSex() == "female").collect(Collectors.toList()).forEach(System.out::println);
					
		Map<String, Long> map = users.stream().collect(Collectors.groupingBy(User::getSex, Collectors.counting()));
		Map<String, Integer> map2 = users.stream().collect(Collectors.groupingBy(User::getSex, Collectors.summingInt(p -> 1)));
		System.out.println("map :" +map.toString());
		System.out.println("map2 :" +map2.toString());
		
		List<String> items = Arrays.asList("apple", "apple", "banana","apple", "orange", "banana", "papaya");
		Map<String, Long> map3 = items.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
		Map<String, Long> finalMap = new LinkedHashMap<>();
		map3.entrySet().stream().sorted(Map.Entry.<String, Long>comparingByValue().reversed()).forEachOrdered(e -> finalMap.put(e.getKey(), e.getValue()));
		System.out.println("finalMap : "+ finalMap);
		
		Map<String, List<User>> mapUser = users.stream().collect(Collectors.groupingBy(User::getSex));
		System.out.println("mapUser : "+ mapUser);
		
		System.out.println("foreach only male name : ");
		users.stream().filter(p -> p.getSex() == "male").map(v -> v.getName()).collect(Collectors.toList()).forEach(System.out::println);
		
		Map<String, Double> userGroupAvg = users.stream().collect(Collectors.groupingBy(User::getSex, Collectors.averagingInt(User::getAge)));
		System.out.println("userGroupAvg : "+ userGroupAvg);
			
		String findAny = users.stream().findAny().get().toString();
		System.out.println("findAny : "+ findAny);

		Student obj1 = new Student();
        obj1.setName("mkyong");
        obj1.addBook("Java 8 in Action");
        obj1.addBook("Spring Boot in Action");
        obj1.addBook("Effective Java (2nd Edition)");

        Student obj2 = new Student();
        obj2.setName("zilap");
        obj2.addBook("Learning Python, 5th Edition");
        obj2.addBook("Effective Java (2nd Edition)");

        List<Student> listStudent = new ArrayList<>();
        listStudent.add(obj1);
        listStudent.add(obj2);
        
        System.out.println("foreach book : ");
        listStudent.stream().map(b -> b.getBook()).flatMap(p -> p.stream()).distinct().forEach(System.out::println);
        
        System.out.println("streamOf : ");
        Stream.of("folk1", "folk2", "folk3").collect(Collectors.toList()).forEach(System.out::println);
        System.out.println("streamIterate : ");
        Stream.iterate(2, p -> p + 1).limit(10).collect(Collectors.toList()).forEach(System.out::println);
        System.out.println("streamGenerate : ");
        Stream.generate(() -> new Random().nextInt(10)).limit(10).collect(Collectors.toList()).forEach(System.out::println);
        
        System.out.println("options : ");
        List<String> nameList = Arrays.asList("Anakin", "Luke", "Darth Vader", "Han Solo", "Stormtrooper", "Cherprang", null);
        Optional<List<String>> listOptional = createOptional(null);
        System.out.println(listOptional.toString());
	}
	
	private static <T> Optional<T> createOptional(T value) {
        return value == null ? Optional.empty() : Optional.of(value);
    }				
}
