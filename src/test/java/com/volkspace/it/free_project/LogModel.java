package com.volkspace.it.free_project;

import java.util.List;

public class LogModel {
	
	private String id;
	
	private String deployment_id;
	
	private String start_process;
	
	private String end_process;
	
	private String log_group;
	
	private String deleted_flag;
	
	private String deleted_date;
	
	private String created_date;
	
	private String count_row;
	
	private String fail_count;
	
	private String success_count;
	
	private List<FailRecord> fail_record;
	
	private String source_name;
	
	public LogModel() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDeployment_id() {
		return deployment_id;
	}

	public void setDeployment_id(String deployment_id) {
		this.deployment_id = deployment_id;
	}

	public String getStart_process() {
		return start_process;
	}

	public void setStart_process(String start_process) {
		this.start_process = start_process;
	}

	public String getEnd_process() {
		return end_process;
	}

	public void setEnd_process(String end_process) {
		this.end_process = end_process;
	}

	public String getLog_group() {
		return log_group;
	}

	public void setLog_group(String log_group) {
		this.log_group = log_group;
	}

	public String getDeleted_flag() {
		return deleted_flag;
	}

	public void setDeleted_flag(String deleted_flag) {
		this.deleted_flag = deleted_flag;
	}

	public String getDeleted_date() {
		return deleted_date;
	}

	public void setDeleted_date(String deleted_date) {
		this.deleted_date = deleted_date;
	}

	public String getCreated_date() {
		return created_date;
	}

	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}

	public String getCount_row() {
		return count_row;
	}

	public void setCount_row(String count_row) {
		this.count_row = count_row;
	}

	public String getFail_count() {
		return fail_count;
	}

	public void setFail_count(String fail_count) {
		this.fail_count = fail_count;
	}

	public String getSuccess_count() {
		return success_count;
	}

	public void setSuccess_count(String success_count) {
		this.success_count = success_count;
	}

	public List<FailRecord> getFail_record() {
		return fail_record;
	}

	public void setFail_record(List<FailRecord> fail_record) {
		this.fail_record = fail_record;
	}

	public String getSource_name() {
		return source_name;
	}

	public void setSource_name(String source_name) {
		this.source_name = source_name;
	}

	@Override
	public String toString() {
		return "LogModel [id=" + id + ", deployment_id=" + deployment_id + ", start_process=" + start_process
				+ ", end_process=" + end_process + ", log_group=" + log_group + ", deleted_flag=" + deleted_flag
				+ ", deleted_date=" + deleted_date + ", created_date=" + created_date + ", count_row=" + count_row
				+ ", fail_count=" + fail_count + ", success_count=" + success_count + ", fail_record=" + fail_record
				+ ", source_name=" + source_name + "]";
	}
}
