package com.volkspace.it.free_project;

import java.io.IOException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.stream.Collectors;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONValue;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

public class AppTest {

	// @Test
	public void whenPostJsonUsingHttpClient_thenCorrect() throws ClientProtocolException, IOException {
		HttpClient httpClient = HttpClients.createDefault();
		String restUrl = URLEncoder.encode(
				"https://core-atv-dev.bizcuitvoc.com/api/core/3/log/listBySourceAndDate/MasterDataImporter/2018-05-10T00:00:00.000Z/2018-05-13T00:00:00.000Z",
				"UTF-8");
		HttpGet httpGet = new HttpGet(restUrl);
		httpGet.addHeader("Authorization",
				"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjUyNmNkZDhmLWE4N2EtNDNmZS1hNmZjLWYxZjkzMmU4NTc4MyIsIm1ldGFkYXRhIjp7fSwiaWF0IjoxNTI0NDU2MzU4LCJqdGkiOiI2YmMxOGRmZi0wODBmLTRiMDQtYTVkOS0xZTIwODdmZTRmMzEifQ.comeFKs2A2ACY-j4k5UIBORwNxA0aZu_erpfecuMJX8");
		HttpResponse response = httpClient.execute(httpGet);
		HttpEntity entity2 = response.getEntity();
		String responseString = EntityUtils.toString(entity2, "UTF-8");
		System.out.println("Response: " + responseString);
		Object obj = JSONValue.parse(responseString);
		JSONArray finalResult = (JSONArray) obj;
		System.out.println(finalResult);
		for (Object object : finalResult) {
			ObjectMapper mapper = new ObjectMapper();
			LogModel logModel = mapper.readValue(object.toString(), LogModel.class);
			System.out.println(logModel.toString());
		}
	}

	// @Test
	public void whenPostJsonUsingHttpClient_thenCorrect2() throws ClientProtocolException, IOException {
		HttpClient httpClient = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet("https://core-atv-dev.bizcuitvoc.com/api/core/3/log/listAllSource");
		httpGet.addHeader("Authorization",
				"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjUyNmNkZDhmLWE4N2EtNDNmZS1hNmZjLWYxZjkzMmU4NTc4MyIsIm1ldGFkYXRhIjp7fSwiaWF0IjoxNTI0NDU2MzU4LCJqdGkiOiI2YmMxOGRmZi0wODBmLTRiMDQtYTVkOS0xZTIwODdmZTRmMzEifQ.comeFKs2A2ACY-j4k5UIBORwNxA0aZu_erpfecuMJX8");
		HttpResponse response = httpClient.execute(httpGet);
		HttpEntity entity2 = response.getEntity();
		String responseString = EntityUtils.toString(entity2, "UTF-8");
		System.out.println("Response: " + responseString);
		Object obj = JSONValue.parse(responseString);
		JSONArray finalResult = (JSONArray) obj;
		List<String> sourceList = new ArrayList<String>();
		for (Object object : finalResult) {
			sourceList.add(object.toString());
			System.out.println(object.toString());
		}
		System.out.println(sourceList.toString());
	}

	@Test
	public void changeFormat() throws ParseException {
		String dateInString = "Tue May 01 00:00:00 ICT 2018";

		SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
		SimpleDateFormat formatter2 = new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ss'Z'");
		Date date = formatter.parse(dateInString);
		date.setHours(23);
		date.setMinutes(59);
		date.setSeconds(59);
		System.out.println(date);	
		System.out.println(formatter2.format(date));
	}

	// @Test
	public void changeDateFormat() throws ParseException {
		String dateInString = "2018-05-02T11:06:25.099Z";

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		SimpleDateFormat formatter2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Date date = formatter.parse(dateInString.replaceAll("Z$", "+0000"));
		System.out.println(date);

		System.out.println("time zone : " + TimeZone.getDefault().getID());
		System.out.println(formatter2.format(date));
	}

	// @Test
	public void getCountVowels() {
		int count = 0;
		String str = "abracadabra";
		String vowels = "aeiou";

		char[] chars = str.toCharArray();

		for (char c : chars) {
			if (vowels.indexOf(c) >= 0) {
				count++;
			}
		}
		System.out.println("Vowels count: " + count);

		// best practice
		// str.replaceAll("(?i)[^aeiou]", "").length();
	}

	// @Test
	public void spinWords() {
		String sentence = "Hey fellow warriors";
		// String[] arrString = sentence.split(" ");
		// String[] resultString = new String[arrString.length];
		//
		//
		// for (int index = 0; index < arrString.length; index++) {
		// if(arrString[index].toCharArray().length >= 5) {
		// StringBuilder sb = new StringBuilder(arrString[index]);
		// resultString[index] = sb.reverse().toString();
		// } else {
		// resultString[index] = arrString[index];
		// }
		// }
		//
		// String result = String.join(" ", resultString);
		String result = Arrays.stream(sentence.split(" "))
				.map(i -> i.length() > 4 ? new StringBuilder(i).reverse().toString() : i)
				.collect(Collectors.joining(" "));

		System.out.println("Spin word result : " + result);
	}

	// @Test
	public void printerError() {
		String input = "aaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbmmmmmmmmmmmmmmmmmmmxyz";

		char[] chars = input.toCharArray();
		int length = chars.length;
		int count = 0;
		for (char c : chars) {
			count += String.valueOf(c).replaceAll("[a-m]", "").length();
		}
		System.out.println(count + "/" + length);

		// best
		// s.chars().filter(c -> c > 'm').count() + "/" + s.length();
	}

	// @Test
	public void morseWords() {
		Map<String, String> morseAlphabet = new HashMap<>();
		morseAlphabet.put("", " ");
		morseAlphabet.put(".-", "A");
		morseAlphabet.put("-...", "B");
		morseAlphabet.put("-.-.", "C");
		morseAlphabet.put("-..", "D");
		morseAlphabet.put(".", "E");
		morseAlphabet.put("..-.", "F");
		morseAlphabet.put("--.", "G");
		morseAlphabet.put("....", "H");
		morseAlphabet.put("..", "I");
		morseAlphabet.put(".---", "J");
		morseAlphabet.put("-.-", "K");
		morseAlphabet.put(".-..", "L");
		morseAlphabet.put("--", "M");
		morseAlphabet.put("-.", "N");
		morseAlphabet.put("---", "O");
		morseAlphabet.put(".--.", "P");
		morseAlphabet.put("--.-", "Q");
		morseAlphabet.put(".-.", "R");
		morseAlphabet.put("...", "S");
		morseAlphabet.put("-", "T");
		morseAlphabet.put("..-", "U");
		morseAlphabet.put("...-", "V");
		morseAlphabet.put(".--", "W");
		morseAlphabet.put("-..-", "X");
		morseAlphabet.put("-.--", "Y");
		morseAlphabet.put("--..", "Z");
		morseAlphabet.put("-----", "0");
		morseAlphabet.put(".----", "1");
		morseAlphabet.put("..---", "2");
		morseAlphabet.put("...--", "3");
		morseAlphabet.put("....-", "4");
		morseAlphabet.put(".....", "5");
		morseAlphabet.put("-....", "6");
		morseAlphabet.put("--...", "7");
		morseAlphabet.put("---..", "8");
		morseAlphabet.put("----.", "9");

		String morseCode = ".... . -.--   .--- ..- -.. .";
		String[] morseArr = morseCode.replace("   ", "  ").split(" ");
		String output = Arrays.stream(morseArr).map(c -> morseAlphabet.get(c)).collect(Collectors.joining(""));
		System.out.println("Output : " + output);
	}
}
